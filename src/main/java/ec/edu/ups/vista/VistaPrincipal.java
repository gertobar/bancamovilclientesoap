package ec.edu.ups.vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.namespace.QName;

import ec.edu.ups.client.Respuesta;
import ec.edu.ups.client.Trans;
import ec.edu.ups.client.TransaccionService;
import ec.edu.ups.client.TransaccionServiceService;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VistaPrincipal extends JFrame {

	private JPanel contentPane;
	private JTextField txtOrigen;
	private JTextField txtDestino;
	private JTextField txtMonto;
	private static final QName SERVICE_NAME = new QName("http://Services.ups.edu.ec/", "TransaccionServiceService");
	static URL wsdlURL = TransaccionServiceService.WSDL_LOCATION;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaPrincipal frame = new VistaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		 
	        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
	            File wsdlFile = new File(args[0]);
	            try {
	                if (wsdlFile.exists()) {
	                    wsdlURL = wsdlFile.toURI().toURL();
	                } else {
	                    wsdlURL = new URL(args[0]);
	                }
	            } catch (MalformedURLException e) {
	                e.printStackTrace();
	            }
	        }
	      
	      
	}
	  TransaccionServiceService ss = new TransaccionServiceService(wsdlURL, SERVICE_NAME);
      TransaccionService port = ss.getTransaccionServicePort();  

	/**
	 * Create the frame.
	 */
	public VistaPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 356);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Transacciones");
		lblNewLabel.setBounds(164, 11, 85, 14);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Cuenta Origen");
		lblNewLabel_1.setBounds(49, 111, 70, 14);
		panel.add(lblNewLabel_1);
		
		final JLabel lblNewLabel_2 = new JLabel("Cuenta Destino");
		lblNewLabel_2.setBounds(49, 164, 85, 14);
		panel.add(lblNewLabel_2);
		lblNewLabel_2.setVisible(false);
		
		JLabel lblNewLabel_3 = new JLabel("Monto");
		lblNewLabel_3.setBounds(56, 223, 46, 14);
		panel.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Tipo Transaccion");
		lblNewLabel_4.setBounds(49, 53, 85, 14);
		panel.add(lblNewLabel_4);
		
		final JComboBox cbxTipo = new JComboBox();
		cbxTipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String v1 = (String) cbxTipo.getSelectedItem().toString();
				//String text = txtMonto.getText(); 
				if(v1 == "tranferencia") {
					txtDestino.setVisible(true);
					lblNewLabel_2.setVisible(true);
	
				}else if(v1 == "Deposito") {
					txtDestino.setVisible(false);
					lblNewLabel_2.setVisible(false);
					
				}else if(v1 == "Retiro") {
					txtDestino.setVisible(false);
					lblNewLabel_2.setVisible(false);
					
				}
			}
		});
		cbxTipo.setBounds(164, 50, 111, 20);
		cbxTipo.addItem("Selecione");
		cbxTipo.addItem("tranferencia");
		cbxTipo.addItem("Deposito");
		cbxTipo.addItem("Retiro");
		panel.add(cbxTipo);
		
		txtOrigen = new JTextField();
		txtOrigen.setBounds(164, 108, 86, 20);
		panel.add(txtOrigen);
		txtOrigen.setColumns(10);
		
		txtDestino = new JTextField();
		txtDestino.setBounds(163, 161, 86, 20);
		panel.add(txtDestino);
		txtDestino.setColumns(10);
		txtDestino.setVisible(false);
		
		txtMonto = new JTextField();
		txtMonto.setBounds(163, 220, 86, 20);
		panel.add(txtMonto);
		txtMonto.setColumns(10);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
				ec.edu.ups.client.Trans t= new Trans();
				Respuesta r  =new Respuesta();
				
				String v1 = (String) cbxTipo.getSelectedItem().toString();
				String text = txtMonto.getText(); 
				double value = Double.parseDouble(text);
				
				
				
				if(v1 == "tranferencia") {
					try {
						txtDestino.setVisible(true);
					t.setCuentaorigen(txtOrigen.getText());
					t.setCuentadestino(txtDestino.getText());
					t.setMonto(value);	
					t.setTipo(v1);
				ec.edu.ups.client.Respuesta _transacciones__return = port.transacciones(t);
				System.out.println("transacciones.result=" + _transacciones__return.toString());
				JOptionPane.showMessageDialog(null, _transacciones__return.toString());
					}catch (Exception ex){
						r.setMensaje(ex.getMessage());
						
						JOptionPane.showMessageDialog(null, "Transaccion Realizada");
						
					}
				
				
				}else if(v1 == "Deposito") {
					t.setCuentaorigen(txtOrigen.getText());
					t.setCuentadestino("");
					t.setMonto(value);
					t.setTipo(v1);
					ec.edu.ups.client.Respuesta _transacciones__return = port.transacciones(t);
					System.out.println("transacciones.result=" + _transacciones__return.toString());
					JOptionPane.showMessageDialog(null, _transacciones__return.toString());
					
				}else if(v1 == "Retiro") {

					t.setCuentaorigen(txtOrigen.getText());
					t.setCuentadestino("");
					t.setMonto(value);
					t.setTipo(v1);
					ec.edu.ups.client.Respuesta _transacciones__return = port.transacciones(t);
					System.out.println("transacciones.result=" + _transacciones__return.toString());
					JOptionPane.showMessageDialog(null, _transacciones__return.toString());
					
				}
				
			}
		});
		btnAceptar.setBounds(92, 273, 89, 23);
		panel.add(btnAceptar);
	}
	

}
