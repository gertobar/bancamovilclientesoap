
package ec.edu.ups.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.ups.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Transacciones_QNAME = new QName("http://Services.ups.edu.ec/", "transacciones");
    private final static QName _TransaccionesResponse_QNAME = new QName("http://Services.ups.edu.ec/", "transaccionesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.ups.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Transacciones }
     * 
     */
    public Transacciones createTransacciones() {
        return new Transacciones();
    }

    /**
     * Create an instance of {@link TransaccionesResponse }
     * 
     */
    public TransaccionesResponse createTransaccionesResponse() {
        return new TransaccionesResponse();
    }

    /**
     * Create an instance of {@link Sesion }
     * 
     */
    public Sesion createSesion() {
        return new Sesion();
    }

    /**
     * Create an instance of {@link Empleado }
     * 
     */
    public Empleado createEmpleado() {
        return new Empleado();
    }

    /**
     * Create an instance of {@link Transaccion }
     * 
     */
    public Transaccion createTransaccion() {
        return new Transaccion();
    }

    /**
     * Create an instance of {@link CuentaAhorro }
     * 
     */
    public CuentaAhorro createCuentaAhorro() {
        return new CuentaAhorro();
    }

    /**
     * Create an instance of {@link Usuario }
     * 
     */
    public Usuario createUsuario() {
        return new Usuario();
    }

    /**
     * Create an instance of {@link Respuesta }
     * 
     */
    public Respuesta createRespuesta() {
        return new Respuesta();
    }

    /**
     * Create an instance of {@link Trans }
     * 
     */
    public Trans createTrans() {
        return new Trans();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Transacciones }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Services.ups.edu.ec/", name = "transacciones")
    public JAXBElement<Transacciones> createTransacciones(Transacciones value) {
        return new JAXBElement<Transacciones>(_Transacciones_QNAME, Transacciones.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransaccionesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Services.ups.edu.ec/", name = "transaccionesResponse")
    public JAXBElement<TransaccionesResponse> createTransaccionesResponse(TransaccionesResponse value) {
        return new JAXBElement<TransaccionesResponse>(_TransaccionesResponse_QNAME, TransaccionesResponse.class, null, value);
    }

}
